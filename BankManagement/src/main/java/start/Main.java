package start;

import java.awt.EventQueue;

import bank.Bank;
import observerDP.ObserverAccount;
import presentation.View;

public class Main {

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		Bank bank = Bank.getInstance();
		bank.atach(new ObserverAccount());
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					View frame = new View();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
