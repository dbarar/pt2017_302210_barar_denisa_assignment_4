package observerDP;

import account.Account;

public abstract class Observer {
	protected Account subject;
	
	public abstract void update(Account account);
}
