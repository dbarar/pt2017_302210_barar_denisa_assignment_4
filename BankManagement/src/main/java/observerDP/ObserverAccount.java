package observerDP;

import account.Account;

public class ObserverAccount extends Observer{

	@Override
	public void update(Account account) {
		System.out.println(account.getState());
	}

}
