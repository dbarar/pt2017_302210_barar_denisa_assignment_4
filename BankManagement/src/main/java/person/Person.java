package person;

import java.util.Objects;

public class Person implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4962248484720208498L;
	
	private String firstName;
	private String lastName;
	private String address;
	private String phone;
	private String CNP;
	
	public Person(String firstName, String lastName, String address, String phone, String CNP){
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
		this.phone = phone;
		this.CNP = CNP;
	}
	public Person(String firstName, String lastName, String address, String phone){
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
		this.phone = phone;
	}
	public Person(){
		this(null, null, null, null, null);
	}
	@Override
	public String toString(){
		return "Person: firstName: " + firstName + " lastName: " + lastName + " address: " + address + " CNP: " + CNP;
	}
	@Override
	public boolean equals(Object o){
		//self check
		if(this == o){
			return true;
		}
		//null check
		if(o == null){
			return false;
		}
		//type check and cast
		if(getClass() != o.getClass()){
			return false;
		}
		Person person = (Person) o;
		//field comparison
		return Objects.equals(firstName, person.firstName)
	            && Objects.equals(lastName, person.lastName)
	            && Objects.equals(CNP, person.CNP);
	}
	@Override
	public int hashCode(){
		int result = 17;
		result = 31 * result + firstName.hashCode();
		result = 31 * result + lastName.hashCode();
		result = 31 * result + CNP.hashCode();		
		return result;
	}
	public boolean isWellFormed(){
		if(firstName == "")
			return false;
		if(lastName == "")
			return false;
		if(phone.length() != 10)
			return false;
		if(CNP.length() != 13)
			return false;
		return true;
	}
	public String getfirstName() {
		return firstName;
	}
	public void setfirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getlastName() {
		return lastName;
	}
	public void setlastName(String lastName) {
		this.lastName = lastName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getCNP() {
		return CNP;
	}
	public void setCNP(String cNP) {
		CNP = cNP;
	}
	
	
}
