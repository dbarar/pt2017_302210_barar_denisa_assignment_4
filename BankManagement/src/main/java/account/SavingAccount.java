package account;

import java.util.Calendar;

import person.Person;

/*The saving account allows a
single large sum deposit and withdrawal and computes an interest during the deposit
period. */
public class SavingAccount extends Account{

	/**
	 * 
	 */
	private static final long serialVersionUID = 9115549739299881185L;

	public SavingAccount(Person person, Calendar date){
		super(person, date);
		setAnnualInterestRate(0.15);
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString() + " tipul: SavingAccount;";
	}
	@Override
	public void withdraw(double money) throws Exception {
		// TODO Auto-generated method stub
		double sum = money + money * getAnnualInterestRate();
		if(getBalance() > sum)
			setBalance(getBalance() - sum);
		else throw new Exception("Nu sunt suficienti bani in depozit!");
	}
	@Override
	public void deposit(double money) {
		// TODO Auto-generated method stub
		setBalance(getBalance() + money);
	}

	
}
