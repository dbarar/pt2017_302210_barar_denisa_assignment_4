package account;

import java.util.Calendar;
import java.util.Objects;

import person.Person;

public abstract class Account implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1615086613944716911L;
	
	
	private static int count = 0; 
	private int id;
	private double balance = 0.0;//balance is always the net amount after factoring in all debits and credits.
	private double annualInterestRate = 0.0;// the interest rate that is actually earned or paid on an investment, loan or other financial product due to the result of compounding over a given time period.
	private Calendar date;
	private Person person;
	
	private String state;
	
	public Account(Person person, Calendar date){
		count++;
		this.id = count;
		this.date = date;
		this.person = person;
	}
	public Account(){
		this(null, null);
	}
	
	public abstract void withdraw(double money) throws Exception;
	public abstract void deposit(double money);
	
	public boolean isWellFormed(){
		if(!person.isWellFormed())
			return false;
		if(id < 0)
			return false;
		if(balance < 0)
			return false;
		if(annualInterestRate < 0)
			return false;
		return true;
	}
	
	@Override
	public String toString(){
		return "Account: id: " + id + " balance: " + " annualIR: " + annualInterestRate + " date: " + date + " owned by: " + person;
	}
	@Override
	public boolean equals(Object o){
		//self check
		if(this == o){
			return true;
		}
		//null check
		if(o == null){
			return false;
		}
		//type check and cast
		if(getClass() != o.getClass()){
			return false;
		}
		Account account = (Account) o;
		//field comparison
		return Objects.equals(id, account.id);
	}
	@Override
	public int hashCode(){
		int result;
		result = 31 * id;
		return result;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public double getAnnualInterestRate() {
		return annualInterestRate;
	}
	public void setAnnualInterestRate(double annualInterestRate) {
		this.annualInterestRate = annualInterestRate;
	}
	public Calendar getDate() {
		return date;
	}
	public void setDate(Calendar date) {
		this.date = date;
	}
	public Person getPerson() {
		return person;
	}
	public void setPerson(Person person) {
		this.person = person;
	}
	public static int getCount() {
		return count;
	}
	public static void setCount(int count) {
		Account.count = count;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
}
