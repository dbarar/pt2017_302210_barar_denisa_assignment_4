package account;

import java.util.Calendar;

import person.Person;

/*The spending account allows several deposits and withdrawals, but does not
compute any interest.*/
public class SpendingAccount extends Account{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 677382046632419881L;
	
	public SpendingAccount(Person person, Calendar date){
		super(person, date);
	}

	@Override
	public void withdraw(double money) throws Exception {
		if(getBalance() > money)
			setBalance(getBalance() - money);
		else throw new Exception("Nu sunt suficienti bani in depozit!");
	}

	@Override
	public void deposit(double money) {
		setBalance(getBalance() + money);
	}
	
	@Override
	public String toString() {
		return super.toString() + " tipul: SpendingAccount;";
	}

}
