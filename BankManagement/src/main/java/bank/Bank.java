package bank;

import java.util.HashMap;
import java.util.List;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import account.Account;
import observerDP.Observer;
import person.Person;

public class Bank implements BankProc{
	
	private static String fileToReadPath = "D:\\PT_Barar_Denisa\\BankManagement\\fileToReadPath.ser";
	private static String fileToWritePath = "D:\\PT_Barar_Denisa\\BankManagement\\fileToReadPath.ser";
	private HashMap<Person, List<Account>> bankData;
	private List<Person> persons;
	private static final Bank singleInstance = new Bank();
	
	private List<Observer> observers = new ArrayList<Observer>();
	
	private Bank(){
		bankData = new HashMap<Person, List<Account>>();
		persons = new ArrayList<Person>();
	}
	
	public static Bank getInstance(){
		return singleInstance;
	}

	public void addPerson(Person person) {
		assert person.isWellFormed();
		
		int sizeBeforeAddPerson = persons.size();
		
		if(!persons.contains(person))
		{
			persons.add(person);
			assert persons.size() == sizeBeforeAddPerson + 1;		
		}		
	}

	public void removePerson(Person person) {
		assert person.isWellFormed();
		
		int sizeBeforeRemovePerson = persons.size();
		
		bankData.remove(person);
		persons.remove(person);
		
		assert persons.size() == sizeBeforeRemovePerson - 1;		
	}
	
	public Person findByCNP(String cnp){
		for(Person p: persons){
			if(p.getCNP().equals(cnp))
				return p;
		}
		return null;
	}
	
	public void editPerson(Person newP){//CNP field not modifiable
		Person oldP = findByCNP(newP.getCNP());
		List<Account> accounts = bankData.get(oldP);
		
		persons.remove(oldP);
		persons.add(newP);		
		
		for(Account a: accounts){
			a.setPerson(newP);
		}
		bankData.remove(oldP);
		bankData.put(newP, accounts);		
	}
	
	public void addAccount(Account account) {
		assert account.isWellFormed();
		
		Person person = account.getPerson();
		int sizeBeforeAddFirstAccount = 0;
		int sizeBeforeAddNewAccount = 0;		
		
		if(!bankData.containsKey(person)){//persoana nu are un cont;
			addPerson(person);
			sizeBeforeAddFirstAccount = bankData.size();
			List<Account> list = new ArrayList<Account>();
			list.add(account);
			bankData.put(person, list);
		}
		else{//persoana are deja un cont
			assert persons.contains(person);
			sizeBeforeAddNewAccount = bankData.get(person).size();
			bankData.get(person).add(account);
		}
		
		account.setState("The owner " + person + "of the account with the id " + account.getId() + "is beeing notified that the account has been created");
		this.notifyObservers(account);
		
		assert bankData.size() == sizeBeforeAddFirstAccount + 1;
		assert bankData.get(person).size() == sizeBeforeAddNewAccount + 1;
	}

	public void removeAccount(Account account) {
		assert account.isWellFormed();
		
		Person person = account.getPerson();
		assert persons.contains(person);
		
		int sizeBucketBeforeRemove = bankData.size();
		int sizeListBeforeRemoveAccount = bankData.get(person).size();		
		
		if(bankData.containsKey(person)){//persoana exista
			if(sizeListBeforeRemoveAccount == 1){//are un cont, il stergem si stergem si persoana
				bankData.get(person).remove(account);
				bankData.remove(person);
				removePerson(person);
				assert  bankData.size() == sizeBucketBeforeRemove - 1;
			}
			else{//stergem doar contul cerut pentru a fi sters
				bankData.get(person).remove(account);
				assert bankData.get(person).size()  == sizeListBeforeRemoveAccount - 1;
			}		
		}
		
		account.setState("The owner " + person + "of the account with the id " + account.getId() + "is beeing notified that the account has been removed");
		this.notifyObservers(account);
	}

	public void readAccountsData() {
		FileInputStream file = null;
		ObjectInputStream in = null;		
		try {
			file = new FileInputStream(fileToReadPath);
		} catch (FileNotFoundException e1) {
			System.out.println("error at opening the FileInputStream");
			e1.printStackTrace();
		}
		try {
			in = new ObjectInputStream(file);
		} catch (IOException e1) {
			System.out.println("error at making the ObjectInputStream");
			e1.printStackTrace();
		}
		try {
			Account.setCount(in.readInt());
			persons = (List<Person>) in.readObject();
			bankData = (HashMap<Person, List<Account>>) in.readObject();
		} catch (ClassNotFoundException e) {
			System.out.println("error at deserealizing");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("error at deserealizing 2");
			e.printStackTrace();
		}
		try {
			in.close();
		} catch (IOException e) {
			System.out.println("error at closing the ObjectInputStream");
			e.printStackTrace();
		}
		try {
			file.close();
		} catch (IOException e) {
			System.out.println("error at closing the FileInputStream");
			e.printStackTrace();
		}
		
		assert bankData != null;
	}

	public void writeAccountData() {
		assert bankData != null;
		
		boolean bankWritten = false;
		
		ObjectOutputStream out= null;		
		FileOutputStream file = null;
		
		try {
			file = new FileOutputStream(fileToWritePath);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		try {
			out = new ObjectOutputStream(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			out.writeInt(Account.getCount());
			out.writeObject(persons);
			out.writeObject(bankData);
			bankWritten = true;
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			file.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		assert bankWritten;
	}

	public Account getAccount(Person person, int crt) {
		List<Account> accounts = bankData.get(person);
		if(accounts == null)
			return null;
		for(Account a: accounts){
			if(a.getId() == crt)
				return a;
		}
		return null;
	}

	public double addMoneyIntoAccount(Person person, Account account, double money) {
		account.deposit(money);
		account.setState("The owner " + person + "of the account with the id " + account.getId() + "is beeing notified that in the account has been added the amount of " + money + "money" );
		this.notifyObservers(account);
		
		return 0;
	}

	public double extractMoneyFromAccount(Person person, Account account, double money) throws Exception {
		account.withdraw(money);
		account.setState("The owner " + person + "of the account with the id " + account.getId() + "is beeing notified that from the account have been removed the amount of " + money + "money" );
		this.notifyObservers(account);
		
		return 0;
	}
	
	public HashMap<Person, List<Account>> getBank() {
		return bankData;
	}

	public void setBank(HashMap<Person, List<Account>> bank) {
		this.bankData = bank;
	}

	public List<Person> getPersons() {
		return persons;
	}

	public void setPersons(List<Person> persons) {
		this.persons = persons;
	}
	
	public void atach(Observer observer){
		this.observers.add(observer);
	}
	
	public void detach(Observer observer){
		this.observers.remove(observer);
	}
	
	public void notifyObservers(Account account){
		this.observers.forEach(observer -> observer.update(account));
	}
}
