package bank;

import account.Account;
import person.Person;

public interface BankProc {
	


	/**
	 * @pre person != null
	 * @post !isEmpty()
	 * @post size = @pre.getSize() + 1;
	 * @param person
	 */
	public void addPerson(Person person);
	
	/**
	 * @pre person != null
	 * @pre list.isEmpty() = false
	 * @post size = @pre.getSize() - 1
	 * @param person
	 */
	public void removePerson(Person person);
	
	/**
	 * @pre account != null
	 * @post !isEmpty()
	 * @post size = @pre.getSize() + 1;
	 * @param account
	 */
	public void addAccount(Account account);
	
	/**
	 * @pre person != null
	 * @pre list.isEmpty() = false
	 * @post size = @pre.getSize() - 1;
	 * @param account
	 */
	public void removeAccount(Account account);
	
	/**
	 * @pre bank != NULL;
	 * @post bank != NULL;
	 */
	public void readAccountsData();
	
	/**
	 * @pre bank != NULL;
	 * 
	 */
	public void writeAccountData();
	
	/**
	 * @pre crt > 0
	 * @pre person != null
	 * @post Account != NULL;
	 * @param person
	 * @param crt
	 * @return
	 */
	public Account getAccount(Person person, int crt);
	
	/**
	 * @pre crt > 0
	 * @pre person != null
	 * @pre money > 0
	 * @post balance > 0
	 * @param person
	 * @param crt
	 * @param money
	 * @return
	 */
	public double addMoneyIntoAccount(Person person, Account account, double money);
	
	/**
	 * @pre crt > 0
	 * @pre person != null
	 * @pre money > 0
	 * @param person
	 * @param crt
	 * @param money
	 * @return
	 */
	public double extractMoneyFromAccount(Person person, Account account, double money) throws Exception;
	
}
