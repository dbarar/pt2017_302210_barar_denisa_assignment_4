package presentation;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import account.Account;
import account.SavingAccount;
import account.SpendingAccount;
import bank.Bank;
import person.Person;

import java.awt.Color;
import javax.swing.JTable;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;

public class AccountView extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8090204933588132772L;
	private JPanel contentPane;
	private JTable table;
	private JTextField depositField;
	private JTextField withdrawField;
	private JTextField dateField;
	private JTextField firstNameField;
	private JTextField lastNameField;
	private JTextField typeField;
	private JTextField balanceField;

	private Person person = View.getSelectedPerson();
	private Account account = View.getSelectedAccount();
	/**
	 * Create the frame.
	 */
	public AccountView() {
		setTitle("Account");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 768, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(102, 255, 102));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		table = new JTable();
		table.setBounds(187, 30, -156, 142);
		contentPane.add(table);
		
		JLabel lblType = new JLabel("TYPE");
		lblType.setBounds(342, 71, 46, 14);
		contentPane.add(lblType);
		
		JLabel lblMakeANew = new JLabel("MAKE A NEW ACCOUNT");
		lblMakeANew.setBounds(342, 9, 209, 14);
		contentPane.add(lblMakeANew);
		
		JLabel lblOperationsOnAccount = new JLabel("OPERATIONS ON ACCOUNT");
		lblOperationsOnAccount.setBounds(342, 101, 183, 14);
		contentPane.add(lblOperationsOnAccount);
		
		JLabel lblQuantity = new JLabel("QUANTITY");
		lblQuantity.setBounds(342, 126, 69, 14);
		contentPane.add(lblQuantity);
		
		JLabel lblQuantity2 = new JLabel("QUANTITY");
		lblQuantity2.setBounds(342, 154, 69, 14);
		contentPane.add(lblQuantity2);
		
		JLabel lblDeleteAccount = new JLabel("DELETE ACCOUNT");
		lblDeleteAccount.setBounds(342, 184, 159, 14);
		contentPane.add(lblDeleteAccount);
		
		depositField = new JTextField();
		depositField.setBounds(407, 123, 144, 20);
		contentPane.add(depositField);
		depositField.setColumns(10);
		
		withdrawField = new JTextField();
		withdrawField.setColumns(10);
		withdrawField.setBounds(407, 152, 144, 20);
		contentPane.add(withdrawField);
		
		JLabel lblDate = new JLabel("DATE");
		lblDate.setBounds(342, 34, 46, 14);
		contentPane.add(lblDate);
		
		dateField = new JTextField();
		dateField.setBounds(407, 30, 144, 20);
		contentPane.add(dateField);
		dateField.setColumns(10);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(204, 255, 0));
		panel.setBounds(10, 13, 288, 88);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblMainHolder = new JLabel("Main Holder:");
		lblMainHolder.setBounds(10, 11, 111, 14);
		panel.add(lblMainHolder);
		
		JLabel lblFirstName = new JLabel("First name:");
		lblFirstName.setBounds(10, 36, 81, 14);
		panel.add(lblFirstName);
		
		JLabel lblLastName = new JLabel("Last name:");
		lblLastName.setBounds(10, 61, 81, 14);
		panel.add(lblLastName);
		
		firstNameField = new JTextField();
		firstNameField.setBounds(120, 33, 158, 20);
		panel.add(firstNameField);
		firstNameField.setColumns(10);
		
		lastNameField = new JTextField();
		lastNameField.setColumns(10);
		lastNameField.setBounds(120, 58, 158, 20);
		panel.add(lastNameField);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(102, 204, 0));
		panel_1.setBounds(10, 126, 288, 92);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblAccount = new JLabel("Account:");
		lblAccount.setBounds(10, 11, 72, 14);
		panel_1.add(lblAccount);
		
		JLabel lblType_1 = new JLabel("Type:");
		lblType_1.setBounds(10, 36, 46, 14);
		panel_1.add(lblType_1);
		
		JLabel lblBalance = new JLabel("Balance:");
		lblBalance.setBounds(10, 61, 92, 14);
		panel_1.add(lblBalance);
		
		typeField = new JTextField();
		typeField.setBounds(123, 33, 155, 20);
		panel_1.add(typeField);
		typeField.setColumns(10);
		
		balanceField = new JTextField();
		balanceField.setColumns(10);
		balanceField.setBounds(123, 58, 155, 20);
		panel_1.add(balanceField);
		
		//initializare fielduri person si account
		firstNameField.setText(person.getfirstName());
		lastNameField.setText(person.getlastName());
		if(account instanceof SavingAccount)
			typeField.setText("Saving account");
		else if(account instanceof SpendingAccount)
			typeField.setText("Spending account");
		balanceField.setText(account.getBalance() + "");
		
		JButton btnSavingAccount = new JButton("Saving Account");
		btnSavingAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Bank bank = Bank.getInstance();
				
				Calendar date = Calendar.getInstance();
				SimpleDateFormat sdf = new SimpleDateFormat("dd-mm-yyyy", Locale.ENGLISH);
				try {
					date.setTime(sdf.parse(dateField.getText()));
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				Account account = new SavingAccount(person, date);
				bank.addAccount(account);
			}
		});
		btnSavingAccount.setBounds(407, 67, 144, 23);
		contentPane.add(btnSavingAccount);
		
		JButton btnSpendingAccount = new JButton("Spending Account");
		btnSpendingAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Bank bank = Bank.getInstance();
				
				Calendar date = Calendar.getInstance();
				SimpleDateFormat sdf = new SimpleDateFormat("dd-mm-yyyy", Locale.ENGLISH);
				try {
					date.setTime(sdf.parse(dateField.getText()));
				} catch (ParseException e1) {
					date.set(2017, 01, 01);
					e1.printStackTrace();
				}
				
				Account account = new SpendingAccount(person, date);
				bank.addAccount(account);
			}
		});
		btnSpendingAccount.setBounds(579, 67, 144, 23);
		contentPane.add(btnSpendingAccount);
		
		JButton btnAddMoney = new JButton("Deposit money");
		btnAddMoney.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				double money = Double.parseDouble(depositField.getText());
				Bank bank = Bank.getInstance();
				bank.addMoneyIntoAccount(person, account, money);
				balanceField.setText(bank.getAccount(person, account.getId()).getBalance() + "");
			}
		});
		btnAddMoney.setBounds(579, 120, 144, 23);
		contentPane.add(btnAddMoney);
		
		JButton btnWithdrawMoney = new JButton("Withdraw money");
		btnWithdrawMoney.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				double money = Double.parseDouble(withdrawField.getText());
				Bank bank = Bank.getInstance();
				try {
					bank.extractMoneyFromAccount(person, account, money);
					balanceField.setText(bank.getAccount(person, account.getId()).getBalance() + "");
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnWithdrawMoney.setBounds(579, 148, 144, 23);
		contentPane.add(btnWithdrawMoney);
		
		JButton btnDeleteAccount = new JButton("Delete account");
		btnDeleteAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Bank bank = Bank.getInstance();
				bank.removeAccount(account);
			}
		});
		btnDeleteAccount.setBounds(579, 195, 144, 23);
		contentPane.add(btnDeleteAccount);
	}
}
