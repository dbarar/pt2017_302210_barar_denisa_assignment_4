package presentation;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import account.Account;
import account.SavingAccount;
import account.SpendingAccount;
import bank.Bank;
import person.Person;

import java.awt.Color;
import javax.swing.JTable;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Vector;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;

public class View extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3487313837768875561L;
	private JPanel contentPane;
	private JTable table;
	private static Person selectedPerson;
	private static Account selectedAccount;

	/**
	 * Create the frame.
	 */
	public View() {
		setTitle("Bank Management");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 797, 388);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(102, 51, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		final JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 20, 459, 318);
		contentPane.add(scrollPane);
		
		JButton btnPerson = new JButton("Person");
		btnPerson.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PersonView personView = new PersonView();
				personView.setVisible(true);
			}
		});
		btnPerson.setBounds(497, 59, 89, 75);
		contentPane.add(btnPerson);
		
		JButton btnAccount = new JButton("Account");
		btnAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AccountView accountView = new AccountView();
				accountView.setVisible(true);
			}
		});
		btnAccount.setBounds(497, 145, 89, 74);
		contentPane.add(btnAccount);
		
		JButton btnSerialize = new JButton("Serialize");
		btnSerialize.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Bank bank = Bank.getInstance();
				bank.writeAccountData();
			}
		});
		btnSerialize.setBounds(669, 145, 89, 74);
		contentPane.add(btnSerialize);
		
		JButton btnDeserialize = new JButton("Deserialize");
		btnDeserialize.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Bank bank = Bank.getInstance();
				bank.readAccountsData();
				table = makeTableOfBankData(bank.getBank());
				scrollPane.setViewportView(table);
			}
		});
		btnDeserialize.setBounds(669, 59, 89, 75);
		contentPane.add(btnDeserialize);
		
		JButton btnRefresh = new JButton("Refresh");
		btnRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Bank bank = Bank.getInstance();
				table = makeTableOfBankData(bank.getBank());
				scrollPane.setViewportView(table);
			}
		});
		btnRefresh.setBounds(497, 265, 261, 60);
		contentPane.add(btnRefresh);
		
	}
	
	
	private JTable makeTableOfBankData(HashMap<Person, List<Account>> bankData){
		final JTable table = new JTable();
		Vector<String> columnNames = null;
		Vector<Vector<Object>> data;
		
		columnNames = new Vector<String>();
		columnNames.add("First Name");
		columnNames.add("Last Name");
		columnNames.add("CNP");
		columnNames.add("typeAccount");
		columnNames.add("idAccount");
		columnNames.add("balance");
		data = new Vector<Vector<Object>>();
		
		Set<Entry<Person, List<Account>>> entrySet = bankData.entrySet();
		for(Entry<Person, List<Account>> entry: entrySet){
			ArrayList<Account> accountsList = (ArrayList<Account>) entry.getValue();
			for(Account account: accountsList){
				Person person = account.getPerson();
				Vector<Object> vectorAuxiliar = new Vector<Object>();
				vectorAuxiliar.add(person.getfirstName());
				vectorAuxiliar.add(person.getlastName());
				vectorAuxiliar.add(person.getCNP());
				if(account instanceof SavingAccount)
					vectorAuxiliar.add(new String("Saving Account"));
				else if(account instanceof SpendingAccount)
					vectorAuxiliar.add(new String("Spending Account"));
				vectorAuxiliar.add(account.getId());
				vectorAuxiliar.add(account.getBalance());
				
				data.add(vectorAuxiliar);
			}
		}
		final TableModel dataModel = (TableModel) new DefaultTableModel(data, columnNames);
		table.setModel(dataModel);
		table.getSelectionModel().addListSelectionListener(
				new ListSelectionListener() {
					public void valueChanged(ListSelectionEvent e) {
						int viewRow = table.getSelectedRow();
						if(viewRow >=0 ){
							Bank bank = Bank.getInstance();
							selectedPerson = bank.findByCNP(dataModel.getValueAt(viewRow, 2).toString());
							selectedAccount = bank.getAccount(selectedPerson, Integer.parseInt(dataModel.getValueAt(viewRow, 4).toString()));
						}
					}
				});
		return table;
	}
	
	public static Person getSelectedPerson() {
		return selectedPerson;
	}


	public static void setSelectedPerson(Person selectedPerson) {
		View.selectedPerson = selectedPerson;
	}


	public static Account getSelectedAccount() {
		return selectedAccount;
	}


	public static void setSelectedAccount(Account selectedAccount) {
		View.selectedAccount = selectedAccount;
	}


}
