package presentation;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import account.Account;
import account.SavingAccount;
import account.SpendingAccount;
import bank.Bank;
import person.Person;

import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Vector;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JScrollPane;

public class PersonView extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4641789112574357916L;
	private JPanel contentPane;
	private JTextField firstNameField;
	private JTextField lastNameField;
	private JTextField addressField;
	private JTextField phoneField;
	private JTextField cnpField;
	private JTable table;
	private JTextField dateField;

	/**
	 * Create the frame.
	 */
	public PersonView() {
		setTitle("Person");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 1046, 291);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(153, 204, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		final JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 31, 363, 210);
		contentPane.add(scrollPane);
		
		Bank bank = Bank.getInstance();
		table = makeTableOfPersons(bank.getPersons());
		scrollPane.setViewportView(table);		
		
		JButton btnAddPerson = new JButton("ADD");
		btnAddPerson.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Person person = new Person(firstNameField.getText(), lastNameField.getText(), addressField.getText(), phoneField.getText(), cnpField.getText());
				Bank bank = Bank.getInstance();
				bank.addPerson(person);
				table = makeTableOfPersons(bank.getPersons());
				scrollPane.setViewportView(table);
			}
		});
		btnAddPerson.setBounds(662, 30, 132, 23);
		contentPane.add(btnAddPerson);
		
		JButton btnNewButton = new JButton("EDIT");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Person person = new Person(firstNameField.getText(), lastNameField.getText(), addressField.getText(), phoneField.getText(), cnpField.getText());
				Bank bank = Bank.getInstance();
				bank.editPerson(person);
				table = makeTableOfPersons(bank.getPersons());
				scrollPane.setViewportView(table);
			}
		});
		btnNewButton.setBounds(662, 72, 132, 23);
		contentPane.add(btnNewButton);
		
		JButton btnDelete = new JButton("DELETE");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Person person = new Person(firstNameField.getText(), lastNameField.getText(), addressField.getText(), phoneField.getText(), cnpField.getText());
				Bank bank = Bank.getInstance();
				bank.removePerson(person);
				table = makeTableOfPersons(bank.getPersons());
				scrollPane.setViewportView(table);
			}
		});
		btnDelete.setBounds(662, 118, 132, 23);
		contentPane.add(btnDelete);
		
		JButton btnView = new JButton("VIEW");
		btnView.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Bank bank = Bank.getInstance();
				table = makeTableOfPersons(bank.getPersons());
				scrollPane.setViewportView(table);
			}
		});
		btnView.setBounds(662, 161, 132, 23);
		contentPane.add(btnView);
		
		JButton btnClearFields = new JButton("CLEAR FIELDS");
		btnClearFields.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				firstNameField.setText("");
				lastNameField.setText("");
				addressField.setText("");
				phoneField.setText("");
				cnpField.setText("");
			}
		});
		btnClearFields.setBounds(662, 203, 132, 23);
		contentPane.add(btnClearFields);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(51, 204, 255));
		panel.setBounds(383, 34, 252, 207);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblName = new JLabel("FirstName");
		lblName.setBounds(19, 16, 62, 14);
		panel.add(lblName);
		
		JLabel lblLastname = new JLabel("LastName");
		lblLastname.setBounds(19, 47, 61, 14);
		panel.add(lblLastname);
		
		JLabel lblAddress = new JLabel("Address");
		lblAddress.setBounds(19, 78, 61, 14);
		panel.add(lblAddress);
		
		JLabel lblPhone = new JLabel("Phone");
		lblPhone.setBounds(19, 109, 47, 14);
		panel.add(lblPhone);
		
		JLabel lblCnp = new JLabel("CNP");
		lblCnp.setBounds(19, 134, 47, 14);
		panel.add(lblCnp);
		
		firstNameField = new JTextField();
		firstNameField.setBounds(91, 13, 149, 20);
		panel.add(firstNameField);
		firstNameField.setColumns(10);
		
		lastNameField = new JTextField();
		lastNameField.setBounds(91, 44, 149, 20);
		panel.add(lastNameField);
		lastNameField.setColumns(10);
		
		addressField = new JTextField();
		addressField.setBounds(91, 75, 149, 20);
		panel.add(addressField);
		addressField.setColumns(10);
		
		phoneField = new JTextField();
		phoneField.setBounds(91, 106, 149, 20);
		panel.add(phoneField);
		phoneField.setColumns(10);
		
		cnpField = new JTextField();
		cnpField.setBounds(91, 131, 149, 20);
		panel.add(cnpField);
		cnpField.setColumns(10);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(51, 204, 0));
		panel_1.setBounds(810, 31, 207, 210);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblDate = new JLabel("Date");
		lblDate.setBounds(10, 24, 46, 14);
		panel_1.add(lblDate);
		
		dateField = new JTextField();
		dateField.setBounds(42, 8, 152, 47);
		panel_1.add(dateField);
		dateField.setColumns(10);
		
		JButton btnSavingAccount = new JButton("SavingAccount");
		btnSavingAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Bank bank = Bank.getInstance();
				Person person = bank.getPersons().get(bank.getPersons().size() - 1);
				Calendar date = Calendar.getInstance();
				SimpleDateFormat sdf = new SimpleDateFormat("dd-mm-yyyy", Locale.ENGLISH);
				try {
					date.setTime(sdf.parse(dateField.getText()));
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				Account account = new SavingAccount(person, date);
				bank.addAccount(account);
			}
		});
		btnSavingAccount.setBounds(10, 66, 184, 64);
		panel_1.add(btnSavingAccount);
		
		JButton btnSpendingaccount = new JButton("SpendingAccount");
		btnSpendingaccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Bank bank = Bank.getInstance();
				Person person = bank.getPersons().get(bank.getPersons().size() - 1);
				Calendar date = Calendar.getInstance();
				SimpleDateFormat sdf = new SimpleDateFormat("dd-mm-yyyy", Locale.ENGLISH);
				try {
					date.setTime(sdf.parse(dateField.getText()));
				} catch (ParseException e1) {
					date.set(2017, 01, 01);
					e1.printStackTrace();
				}
				Account account = new SpendingAccount(person, date);
				bank.addAccount(account);
			}
		});
		btnSpendingaccount.setBounds(10, 141, 184, 58);
		panel_1.add(btnSpendingaccount);
		
		
	}
	
	private JTable makeTableOfPersons(List<Person> persons){
		final JTable table = new JTable();
		Vector<String> columnNames = null;
		TableModel dataModel = null;
		Vector<Vector<Object>> data;
		
		columnNames = new Vector<String>();
		columnNames.add("First Name");
		columnNames.add("Last Name");
		columnNames.add("Address");
		columnNames.add("Phone");
		columnNames.add("CNP");
		
		data = new Vector<Vector<Object>>();
		for(Person person: persons){
			Vector<Object> vectorAuxiliar = new Vector<Object>();
			vectorAuxiliar.add(person.getfirstName());
			vectorAuxiliar.add(person.getlastName());
			vectorAuxiliar.add(person.getAddress());
			vectorAuxiliar.add(person.getPhone());
			vectorAuxiliar.add(person.getCNP());
			
			data.add(vectorAuxiliar);
		}
		
		dataModel = (TableModel) new DefaultTableModel(data, columnNames);
		table.setModel(dataModel);
		table.getSelectionModel().addListSelectionListener(
				new ListSelectionListener() {
					public void valueChanged(ListSelectionEvent e) {
						int viewRow = table.getSelectedRow();
						if(viewRow < 0){
							firstNameField.setText("");
							lastNameField.setText("");
							addressField.setText("");
							phoneField.setText("");
							cnpField.setText("");
						}else{
							Person p = getPersonFromTableGivenRow(viewRow);
							firstNameField.setText(p.getfirstName());
							lastNameField.setText(p.getlastName());
							addressField.setText(p.getAddress());
							phoneField.setText(p.getPhone());
							cnpField.setText(p.getCNP());
						}
					}
				});
		return table;
	}
	
	private Person getPersonFromTableGivenRow(int row){
		Person p = new Person();
		TableModel dataModel = table.getModel();
		p.setfirstName(dataModel.getValueAt(row, 0).toString());
		p.setlastName(dataModel.getValueAt(row, 1).toString());
		p.setAddress(dataModel.getValueAt(row, 2).toString());
		p.setPhone(dataModel.getValueAt(row, 3).toString());
		p.setCNP(dataModel.getValueAt(row, 4).toString());
		
		return p;
	}
}
