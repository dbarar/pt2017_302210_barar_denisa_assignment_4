import java.util.Calendar;

import account.Account;
import account.SpendingAccount;
import bank.Bank;
import person.Person;

public class Test {

	@org.junit.Test
	public void testAddPerson() {
		Bank bank = Bank.getInstance();
		Person person = new Person("Ana", "Pop", "Cluj", "0123456789", "1234567890123");
		bank.addPerson(person);
		Person personTest = bank.findByCNP("1234567890123");
		assert(person.equals(personTest));
	}

	@org.junit.Test
	public void testRemovePerson() {
		Bank bank = Bank.getInstance();
		Person person = new Person("Ana", "Pop", "Cluj", "0123456789", "1234567890123");
		bank.addPerson(person);
		bank.removePerson(person);
		assert(bank.findByCNP("1234567890123") == null);
	}
	
	@org.junit.Test
	public void testAddAccount() {
		Bank bank = Bank.getInstance();
		Person person = new Person("Ana", "Pop", "Cluj", "0123456789", "1234567890123");
		bank.addPerson(person);
		Calendar date = Calendar.getInstance();
		date.setTimeZone(null);
		Account account = new SpendingAccount(person, date);
		bank.addAccount(account);
		assert(bank.getAccount(person, account.getId()).equals(account));
	}
	
	@org.junit.Test
	public void testRemoveAccount() {
		Bank bank = Bank.getInstance();
		Person person = new Person("Ana", "Pop", "Cluj", "0123456789", "1234567890123");
		bank.addPerson(person);
		Calendar date = Calendar.getInstance();
		date.setTimeZone(null);
		Account account = new SpendingAccount(person, date);
		bank.addAccount(account);
		bank.removeAccount(account);
		assert(bank.getAccount(person, account.getId()) == null);
	}
}
